import axios from 'axios'
import dotenv from 'dotenv'
import { ctxManager } from '../context.mjs'
import { logger } from '../logger.mjs'

dotenv.config()

const instance = axios.create({
  baseURL: process.env.API_HOST,
  headers: { Authorization: `Bearer ${process.env.API_TOKEN}` },
})

export const saveQuestion = async () => {
  try {
    const context = ctxManager.getContext()
    const data = {
      name: context.name,
      phoneNumber: context.phone,
      question: context.question,
      status: 'new',
    }
    const response = (await instance.post('/question', data)).json()
    // PENDING ???????
    return instance.post('/question', data)
  } catch (err) {
    // logger.log('telegram_technical', err)
    console.log('err')
  }
}
