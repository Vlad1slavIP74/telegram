import { papyrus } from '../papyrus.mjs'
import { keyboard, commands } from '../markup.mjs'
import { logger } from '../logger.mjs'
import { ctxManager } from '../context.mjs'
import { validation } from '../validation.mjs'
import { saveQuestion } from './inquisitive.mjs'

const intro = async (ctx, Markup) => {
  try {
    const name = validation.isCorrectName(ctx.from.first_name) ? ctx.from.first_name : null
    await ctxManager.setContext({
      scope: commands.intro,
      previousScope: ctxManager.getContext('scope'),
      name,
    })
    ctx.reply(
      papyrus.getGreeting(name),
      Markup.keyboard(keyboard.intro)
        .oneTime()
        .resize()
        .extra(),
    )
  } catch (e) {
    logger.info(e.stack)
  }
}

const toShockingFacts = async (ctx, Markup) => {
  try {
    await ctxManager.setContext({
      scope: commands.toExpandedTree,
      previousScope: ctxManager.getContext('scope'),
    })
    ctx.reply(
      papyrus.toShockingFacts().text,
      Markup.keyboard(keyboard.toExpandedTree)
        .oneTime()
        .resize()
        .extra(),
    )
  } catch (e) {
    logger.info(e.stack)
  }
}
const goBack = async (ctx, Markup) => {
  try {
    const parentNode = ctxManager.getParentNode()
    if (parentNode) {
      ctxManager.setContext({
        scope: parentNode.scope,
        previousScope: parentNode.previousScope,
      })
      ctx.reply(
        'Назад',
        Markup.keyboard(parentNode.keyboard)
          .oneTime()
          .resize()
          .extra(),
      )
    } else {
      ctx.reply(
        'Назад',
        Markup.keyboard(keyboard.toExpandedTree)
          .oneTime()
          .resize()
          .extra(),
      )
    }
  } catch (e) {
    logger.error(e.stack)
  }
}
const Next = async (ctx, Markup) => {
  try {
    await ctxManager.setContext({
      scope: commands.Next,
      previousScope: ctxManager.getContext('scope'),
    })
    ctx.reply(
      'Продовжити',
      Markup.keyboard(keyboard.toExpandedTree)
        .oneTime()
        .resize()
        .extra(),
    )
  } catch (e) {
    logger.error(e)
  }
}

const askQuestion = async (ctx, Markup, Extra) => {
  try {
    await ctxManager.setContext({
      scope: commands.askQuestion,
      previousScope: ctxManager.getContext('scope'),
    })
    const { phone, name } = ctxManager.getContext()
    if (phone && name) ctx.reply(papyrus.askQuestion)
    if (!name) {
      await ctxManager.setContext({
        scope: commands.askName,
        previousScope: ctxManager.getContext('scope'),
      })
      ctx.reply(
        papyrus.askName,
        Markup.keyboard(keyboard.goBack)
          .oneTime()
          .resize()
          .extra(),
      )
    }
    if (!phone && name) {
      await ctxManager.setContext({
        scope: commands.askPhone,
        previousScope: ctxManager.getContext('scope'),
      })
      ctx.reply(
        'Пришли мне свой номер телефона?)',
        Extra.markdown().markup(markup => {
          return markup.resize().keyboard([markup.contactRequestButton('Отправить контакт')])
        }),
      )
    }
  } catch (e) {
    logger.info(e)
  }
}

const setName = async (ctx, Markup, Extra) => {
  await ctxManager.setContext({
    name: ctx.message.text,
    scope: commands.askPhone,
    previousScope: ctxManager.getContext('scope'),
  })
  ctx.reply(
    papyrus.askPhone,
    Extra.markdown().markup(markup => {
      return markup
        .resize()
        .keyboard([markup.contactRequestButton('Отправить контакт')], ['◀️Назад'])
    }),
  )
}

const setPhone = async (ctx, Markup) => {
  const phone = ctx.message.contact ? ctx.message.contact.phone_number.slice(1) : ctx.message.text
  await ctxManager.setContext({
    phone,
    scope: commands.askQuestion,
    previousScope: ctxManager.getContext('scope'),
  })
  ctx.reply(
    papyrus.askQuestion,
    Markup.keyboard(keyboard.goBack)
      .oneTime()
      .resize()
      .extra(),
  )
}

const setQuestion = async (ctx, Markup) => {
  await ctxManager.setContext({
    scope: commands.setQuestion,
    previousScope: ctxManager.getContext('scope'),
    question: ctx.message.text,
  })
  const res = saveQuestion()

  ctx.reply(
    res ? papyrus.getSuccessQuestion : papyrus.errorOnQuestion,
    Markup.keyboard(keyboard.toExpandedTree)
      .oneTime()
      .resize()
      .extra(),
  )
  await ctxManager.setContext({
    scope: commands.toExpandedTree,
    previousScope: commands.toExpandedTree,
  })
}

const Factory2 = param => {
  return async (ctx, Markup) => {
    try {
      await ctxManager.setContext({
        scope: commands[param],
        previousScope: ctxManager.getContext('scope'),
      })
      const keyboardTodisplay = keyboard[param] || keyboard.goBack
      ctx.reply(
        papyrus[param],
        Markup.keyboard(keyboardTodisplay)
          .oneTime()
          .resize()
          .extra(),
      )
    } catch (e) {
      logger.error(e)
    }
  }
}

export const scenarious = new Map()

scenarious.set('start', intro)
scenarious.set('Цікаві факти', toShockingFacts)
scenarious.set('◀️Назад', goBack)
scenarious.set('⚙️Наши услуги', Factory2('ourServices'))
scenarious.set('📂Каталог услуг', Factory2('servicesCatalog'))
scenarious.set('🧾Цены и сроки выполнения', Factory2('pricesTerms'))
scenarious.set('🛠Поддержка и хостинг', Factory2('hostingSupport'))
scenarious.set('📊Стратегии использования ботов', Factory2('usingStrategies'))
scenarious.set('🧪Сферы применения', Factory2('applianceSpheres'))
scenarious.set('🤞Недостатки и риски', Factory2('risks'))
scenarious.set('💪Преимущества', Factory2('benefits'))
scenarious.set('🛰Связаться с нами', Factory2('contactUs'))
scenarious.set('📒Контакты', Factory2('contacts'))
scenarious.set('📇Блог', Factory2('blog'))
scenarious.set('Продовжити', Next)
scenarious.set('❓Задать вопрос', askQuestion)
scenarious.set('setName', setName)
scenarious.set('setPhone', setPhone)
scenarious.set('setQuestion', setQuestion)
