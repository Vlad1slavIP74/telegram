import { commands } from './markup.mjs'
// eslint-disable-next-line import/no-cycle
import { ctxManager } from './context.mjs'

const needed = [commands.askName, commands.askQuestion, commands.askPhone]

const isCorrectName = name => /^([а-яієїА-ЯІЄёЁЇa-zA-Z]){3,}$/i.test(name)
const isPhoneNumber = phone =>
  /^\d{12}$/.test(phone) && needed.includes(ctxManager.getContext().scope)
const isName = name => isCorrectName(name) && needed.includes(ctxManager.getContext().scope)

const isCustomQuestion = q => {
  const { phone, ...restContext } = ctxManager.getContext()
  console.log(q)
  return (
    /^[.,?!;:'"*/)(+-а-яієїА-ЯІЄЇa-zA-Z\w\s]{7,}$/i.test(q) &&
    (restContext.scope === 'askPhone' || restContext.scope === commands.askQuestion) &&
    phone.length > 0
  )
}

const unexpectedText = message => {
  return !isName(message) && !isPhoneNumber(message)
}

export const validation = {
  isCorrectName,
  isPhoneNumber,
  isName,
  isCustomQuestion,
  unexpectedText,
}
