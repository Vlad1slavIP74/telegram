export const commands = {
  intro: 'intro',
  continue: 'continue',
  toShockingFacts: 'toShockingFacts',
  ourServices: 'ourServices',
  toExpandedTree: 'toExpandedTree',
  servicesCatalog: 'servicesCatalog',
  pricesTerms: 'pricesTerms',
  hostingSupport: 'hostingSupport',
  usingStrategies: 'usingStrategies',
  applianceSpheres: 'applianceSpheres',
  benefits: 'benefits',
  contactUs: 'contactUs',
  contacts: 'contacts',
  blog: 'blog',
  askQuestion: 'askQuestion',
  askName: 'askName',
  askPhone: 'askPhone',
  setQuestion: 'setQuestion',
}

export const keyboard = {
  intro: [['Цікаві факти'], ['Продовжити']],
  toExpandedTree: [
    ['⚙️Наши услуги', '📊Стратегии использования ботов'],
    ['🛰Связаться с нами', '❓Задать вопрос'],
  ],
  goBack: [['◀️Назад']],
  ourServices: [
    ['📂Каталог услуг', '🧾Цены и сроки выполнения'],
    ['🛠Поддержка и хостинг', '❓Задать вопрос'],
    ['◀️Назад'],
  ],
  usingStrategies: [
    ['🧪Сферы применения', '💪Преимущества'],
    ['🤞Недостатки и риски', '❓Задать вопрос'],
    ['◀️Назад'],
  ],
  contactUs: [['📒Контакты', '📇Блог'], ['❓Задать вопрос', '◀️Назад']],
}
