import { commands, keyboard } from './markup.mjs'
import { papyrus } from './papyrus.mjs'

const contextTree = [
  {
    scope: commands.intro,
    previousScope: commands.intro,
    keyboard: keyboard.intro,
    papyrus: papyrus.toShockingFacts,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.Next,
    previousScope: commands.Next,
    keyboard: keyboard.intro,
    papyrus: 'Продовжити',
    inputMethod: ['buttons'],
  },
  {
    scope: commands.toShockingFacts,
    previousScope: commands.intro,
    keyboard: keyboard.intro,
    papyrus: papyrus.toShockingFacts,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.toExpandedTree,
    previousScope: commands.toShockingFacts,
    keyboard: keyboard.toExpandedTree,
    papyrus: papyrus.toExpandedTree,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.ourServices,
    previousScope: commands.toExpandedTree,
    keyboard: keyboard.ourServices,
    papyrus: papyrus.ourServices,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.servicesCatalog,
    previousScope: commands.ourServices,
    keyboard: keyboard.goBack,
    papyrus: papyrus.servicesCatalog,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.pricesTerms,
    previousScope: commands.ourServices,
    keyboard: keyboard.goBack,
    papyrus: papyrus.pricesTerms,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.hostingSupport,
    previousScope: commands.ourServices,
    keyboard: keyboard.goBack,
    papyrus: papyrus.hostingSupport,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.usingStrategies,
    previousScope: commands.toExpandedTree,
    keyboard: keyboard.usingStrategies,
    papyrus: papyrus.usingStrategies,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.applianceSpheres,
    previousScope: commands.usingStrategies,
    keyboard: keyboard.goBack,
    papyrus: papyrus.applianceSpheres,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.benefits,
    previousScope: commands.usingStrategies,
    keyboard: keyboard.goBack,
    papyrus: papyrus.benefits,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.risks,
    previousScope: commands.usingStrategies,
    keyboard: keyboard.goBack,
    papyrus: papyrus.risks,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.contactUs,
    previousScope: commands.toExpandedTree,
    keyboard: keyboard.contactUs,
    papyrus: papyrus.contactUs,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.contacts,
    previousScope: commands.contactUs,
    keyboard: keyboard.goBack,
    papyrus: papyrus.contacts,
    inputMethod: ['buttons'],
  },
  {
    scope: commands.blog,
    previousScope: commands.contactUs,
    keyboard: keyboard.goBack,
    papyrus: papyrus.blog,
    inputMethod: ['buttons'],
  },
]

class Context {
  constructor() {
    this.events = {}
    this.ctx = {
      scope: null,
      previousScope: null,
      question: null,
      name: null,
      phone: null,
      contactWay: null,
    }
    this.tree = contextTree
  }

  getContext(property = false) {
    if (property) {
      if (Reflect.has(this.ctx, property)) return this.ctx[property]
      return null
    }
    return this.ctx
  }

  setContext(obj) {
    Object.keys(obj).forEach(item => {
      if (Reflect.has(this.ctx, item)) return (this.ctx[item] = obj[item])
      return null
    })
  }

  clearContext() {
    Object.keys(this.ctx).forEach(item => (this.ctx[item] = null))
  }

  allowUserInput(textStatus = false) {
    this.ctx.commandsOnly = !textStatus
  }

  checkUserInputAvailability(scope) {
    if (!scope) return null
    const inputAvailability = this.tree.find(node => node.scope === scope).inputMethod
    return inputAvailability || null
  }

  getNode(scope) {
    if (!scope) return null
    return this.tree.find(node => node.scope === scope) || null
  }

  getParentNode() {
    if (!this.ctx.previousScope) return null
    return this.tree.find(node => node.scope === this.ctx.previousScope) || null
  }

  getAll() {
    return this.ctx
  }
}
export const ctxManager = new Context()
