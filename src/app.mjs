import dotenv from 'dotenv'
import Things from 'telegraf'

import Markup from 'telegraf/markup'

import { ctxManager } from './helpers/context.mjs'
import { logger, scenarious, commands, validation, papyrus } from './helpers/index.mjs'

const Telegraf = Things
const { Extra } = Things

console.log(Extra)
dotenv.config()

const bot = new Telegraf(process.env.BOT_ACCOUNT_TOKEN)
try {
  bot.telegram.setWebhook(process.env.URL)
  bot.startWebhook('/', null, process.env.PORT)
} catch (err) {
  bot.telegram.deleteWebhook()
}

bot.start(async ctx => {
  await ctxManager.setContext({
    scope: commands.intro,
    previousScope: commands.intro,
  })
  // console.log(ctx.from);
  scenarious.get('start')(ctx, Markup, Extra)
})

bot.on('message', ctx => {
  const { scope, ...rest } = ctxManager.getContext()

  if (
    scope === 'askName' ||
    scope === 'askPhone' ||
    scope === 'askQuestion' ||
    scenarious.has(ctx.message.text)
  ) {
    if (scope === 'askName') {
      validation.isName(ctx.message.text)
        ? scenarious.get('setName')(ctx, Markup, Extra)
        : ctx.reply(papyrus.invalidName)
    }
    if (scope === 'askPhone') {
      const contact = ctx.message.contact
        ? ctx.message.contact.phone_number.slice(1)
        : ctx.message.text
      validation.isPhoneNumber(contact)
        ? scenarious.get('setPhone')(ctx, Markup)
        : ctx.reply(papyrus.invalidPhoneNumber)
    }
    if (scope === 'askQuestion') {
      validation.isCustomQuestion(ctx.message.text)
        ? scenarious.get('setQuestion')(ctx, Markup, Extra)
        : ctx.reply(papyrus.questionLimitation)
    }
    if (scenarious.has(ctx.message.text)) {
      scenarious.get(ctx.message.text)(ctx, Markup, Extra)
    }
  } else {
    console.log(rest.keyboard)
    ctx.reply(
      papyrus.errorForTyping,
      Markup.keyboard(rest.keyboard)
        .oneTime()
        .resize()
        .extra(),
    )
  }
})

bot.launch()
